# Dockerfile for jackpot image processor image
FROM ubuntu:16.04
MAINTAINER Ali Nebi

#### Small changes to fix build process problems
# Set environment variable to noninteractive only for build process
ARG DEBIAN_FRONTEND=noninteractive

# Avoid ERROR: invoke-rc.d: policy-rc.d denied execution of start.
RUN sed -i "s/^exit 101$/exit 0/" /usr/sbin/policy-rc.d

# Start installatin process
USER root
RUN apt-get update

# Install APT related packages
RUN apt-get install -y apt-utils software-properties-common apt-transport-https

# install development packages
RUN apt-get install -y build-essential

# Install additional required development packages and libraries
RUN apt-get -y install libleptonica-dev libtesseract-dev tesseract-ocr libxxf86vm-dev libglu1-mesa libglu1-mesa-dev libgl1-mesa-glx libgl1-mesa-dev libqt5opengl5-dev libpng16-dev libxinerama-dev libxrandr-dev libxcursor-dev libxi-dev libjasper-dev libopenexr22 libilmbase12 libdc1394-22 libavcodec-ffmpeg-extra56 ffmpeg

RUN apt-get -y install libopencv-dev libopencv-objdetect-dev libopencv-highgui-dev libopencv-legacy-dev libopencv-contrib-dev libopencv-videostab-dev libopencv-superres-dev libopencv-ocl-dev libcv-dev libhighgui-dev libcvaux-dev libgtk2.0-dev libpng-dev

# Install rendering related packages
RUN apt-get install -y xfonts-75dpi xfonts-base fontconfig

# Install tools
RUN apt-get install -y iputils-ping sudo wget vim nano curl git screen locales

### Create 'devuser' user
RUN groupadd -g 1000 devuser
RUN useradd -u 1000 -g 1000 -ms /bin/bash devuser

COPY ssh-keys /home/devuser/.ssh

RUN chmod 0700 /home/devuser/.ssh
RUN if [ ! -n "$(grep "^bitbucket.org " /home/devuser/.ssh/known_hosts)" ]; then ssh-keyscan bitbucket.org >> /home/devuser/.ssh/known_hosts 2>/dev/null; fi

RUN chown -R devuser:devuser /home/devuser
RUN chmod 0600 /home/devuser/.ssh/id_rsa

# Add 'frappe' user to sudoers
RUN printf '# User rules for devuser\ndevuser ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers.d/devuser

# Config changes to system
# Set timezone to Madrid
#RUN echo "Europe/Madrid" > /etc/timezone
#RUN dpkg-reconfigure -f noninteractive tzdata
# ubuntu 16.04
#RUN timedatectl set-timezone Europe/Madrid

# Generate missing localtes
#RUN locale-gen en_US.UTF-8 es_ES.UTF-8
#ADD config/default-locale /etc/default/locale

USER devuser
WORKDIR /home/devuser

# Checkout Jackpot Project and compile
RUN git clone git@bitbucket.org:funktia/jackpotdetection2.git

WORKDIR /home/devuser/jackpotdetection2/jackpotRecognitionSurfBased/jackpotRecognition
RUN /bin/bash -c "source setEnvironment.sh"
RUN /bin/bash buildScript.sh
